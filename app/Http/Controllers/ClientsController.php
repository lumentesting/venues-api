<?php

namespace App\Http\Controllers;

use App\Models\VenuesModel;
use Illuminate\Http\Request;

class ClientsController extends Controller
{

    public function __construct()
    {
        //
    }


    public function addVenue(Request $request)
    {
        if (VenuesModel::addVenueAndTheOwner($request) == false) {
            return responder()->error(406,"Venue " .$request['venueName']. " already has is's owner.")->respond(406);
        }

        $data = [
            'message' => "You are now the venue owner of ".$request['venueName'],
        ];

        return responder()->success($data);
    }


    public function getVenues(Request $request)
    {
        $result = VenuesModel::getVenues($request);

        $data = [
            'venueList' => $result
        ];

        if ($result != false) {
            return responder()->success($data);
        }
    }

}
