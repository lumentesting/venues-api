<?php

namespace App\Http\Middleware;

use App\Helpers\Helpers;
use Closure;

class ClientAccessMiddleware
{

    public function handle( $request, Closure $next )
    {
        $headers = [
            'method'           => $request->method(),
            'token'            => $request->header('token'),
            'external-api-key' => $request->header('api-key')
        ];

        $data = [
            'method'  => 'POST',
            'app'     => 'ACCOUNTS',
            'route'   => 'clients/checkClientToken',
            'headers' => $headers
        ];


        $checkAuth = Helpers::callOtherApi($data);
        $checkAuth = json_decode($checkAuth['body'], true);


        if ($checkAuth['status'] == 200) {
            return $next($request);
        }
        else {
            return responder()->error(406, $checkAuth['error']['message'])->respond(406);
        }

    }
}
