<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VenuesModel extends Model
{
    protected $table = 'venues';

    protected $fillable = [
        'venue_name', 'venue_owner'
    ];


    public static function addVenueAndTheOwner($user)
    {
        $data['venue_name'] = $user['venueName'];
        $data['venue_owner'] = $user['email'];

        $checkVenue = DB::table('venues')->where('venue_name', $data['venue_name'])->first();

        if (empty($checkVenue)) {

            VenuesModel::create($data);

            return true;
        } else {
            return false;
        }

    }

    public static function getVenues($params)
    {
        $venues = DB::table('venues')->where('venue_owner', $params['email'])->get();

        if ($venues->isEmpty()) {
            return false;
        } else {
            $venueData = $venues->map(function ($venue){
                return [
                    'id'   => $venue->id,
                    'name' => $venue->venue_name
                ];
            });

            return $venueData;
        }
    }

}
